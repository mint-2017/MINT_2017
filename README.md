# MINT 2017
MINT 2017 is a repository for safely storing our code.

## Contribution Rules (check these before making a pull request)
1. Don't harm anything.
2. Create branches for everything you wish to do.
3. Do only one thing per commit.
4. Make sure the code builds before you push.
5. Do not push builds.
6. Keep your maximum line width around 100 characters (don't compromise readability to follow this rule).
7. Avoid reformatting unless given the permission to do so.
8. Document your code using [javadoc style](http://www.oracle.com/technetwork/articles/java/index-137868.html).