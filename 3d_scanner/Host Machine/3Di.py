#!/usr/bin/env python

# Work in progress

import pygtk, gtk, vlc
pygtk.require('2.0')

def getCameras():
	# TODO
	print 'Do something'

def getMicrocontroller():
	# TODO
	print ''

class ThreeDi:
	def delete_event(self, widget, event, data=None):
		return False

	def delete(self, widget, data=None):
		gtk.main_quit()

	def cameraL_selector_changed(self, widget, data=None):
		# TODO: Register camera change
		print ''

	def cameraR_selector_changed(self, widget, data=None):
		# TODO: Register camera change
		print ''

	def connect_to_mcu(self, widget, data=None):
		# TODO: Connect to MCU
		print ''

	def start_scan(self, widget, data=None):
		# TODO: Connect to MCU
		print ''

	def start_calibrate(self, widget, data=None):
		# TODO: Connect to MCU
		print ''

	def cameraLView_realize(self, widget, data=None):
		# TODO: Realize left camera
		print ''

	def cameraRView_realize(self, widget, data=None):
		# TODO: Realize right camera
		print ''

	def __init__(self):
		self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)

		self.window.connect('delete_event', self.delete_event)
		self.window.connect('destroy', self.delete)

		self.window.set_border_width(10)

		# Selectors
		self.cameraLSelectorFrame = gtk.Frame(label='Select Left Camera')
		self.cameraLSelectorFrame.show()
		self.cameraLSelectorCombobox = gtk.combo_box_new_text()
		# TODO: Get possible cameras, and add them to the combobox
		self.cameraLSelectorCombobox.connect('changed', self.cameraL_selector_changed)
		self.cameraLSelectorFrame.add(self.cameraLSelectorCombobox)
		self.cameraLSelectorCombobox.show()

		self.cameraRSelectorFrame = gtk.Frame(label='Select Right Camera')
		self.cameraRSelectorFrame.show()
		self.cameraRSelectorCombobox = gtk.combo_box_new_text()
		# TODO: Get possible cameras, and add them to the combobox
		self.cameraRSelectorCombobox.connect('changed', self.cameraR_selector_changed)
		self.cameraRSelectorFrame.add(self.cameraRSelectorCombobox)
		self.cameraRSelectorCombobox.show()

		self.microcontrollerFrame = gtk.Frame(label='Select Microcontroller')
		self.microcontrollerFrame.show()
		self.microcontrollerCombobox = gtk.combo_box_new_text()
		# TODO: Get possible cameras, and add them to the combobox
		self.microcontrollerCombobox.connect('changed', self.cameraR_selector_changed)
		self.microcontrollerFrame.add(self.microcontrollerCombobox)
		self.microcontrollerCombobox.show()

		# Connection Button
		self.connectButton = gtk.Button(label='Connect')
		self.connectButton.connect('pressed', self.connect_to_mcu)
		self.connectButton.show()

		# Calibrate Button
		self.calibrateButton = gtk.Button(label='Calibrate')
		self.calibrateButton.connect('pressed', self.start_calibrate)
		self.calibrateButton.show()

		# Scan Button
		self.scanButton = gtk.Button(label='Scan')
		self.scanButton.connect('pressed', self.start_scan)
		self.scanButton.show()

		# Controls Vbox
		self.controlsVBox = gtk.VBox()
		self.controlsVBox.add(self.cameraLSelectorFrame)
		self.controlsVBox.add(self.cameraRSelectorFrame)
		self.controlsVBox.add(self.microcontrollerFrame)
		self.controlsVBox.add(self.connectButton)
		self.controlsVBox.add(self.scanButton)
		self.controlsVBox.add(self.calibrateButton)
		self.controlsVBox.show()

		# Camera views
		#self.cameraLView = VLCWidget()
		#self.cameraLView.set_size_request(300, 300)
		#self.cameraLView.player.set_media(instance.media_new('~/Downloads/Sam.mp4'))
		#self.cameraLView.show()

		#self.cameraRView = VLCWidget()
		#self.cameraRView.set_size_request(300, 300)
		#self.cameraRView.connect('realize', self.cameraRView_realize)
		#self.cameraRView.show()

		# View
		self.viewHBox = gtk.HBox()
		self.viewHBox.add(self.cameraLView)
		self.viewHBox.add(self.cameraRView)
		self.viewHBox.show()

		# Main content HBox
		self.contentHBox = gtk.HBox()
		self.contentHBox.add(self.controlsVBox)
		self.contentHBox.add(self.viewHBox)
		self.contentHBox.show()

		# Info label
		self.infoLabel = gtk.Label('Info: ')
		self.infoLabel.justify = gtk.JUSTIFY_LEFT
		self.infoLabel.show()

		# Main VBox
		self.mainVBox = gtk.VBox()
		self.mainVBox.add(self.contentHBox)
		self.mainVBox.add(self.infoLabel)
		self.mainVBox.show()

		self.window.add(self.mainVBox)

		self.window.show()

	def main(self):
		gtk.main()

if __name__ == '__main__':
	main_window = ThreeDi()
	main_window.main()