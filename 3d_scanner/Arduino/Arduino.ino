/**
 *  Created by Marko Vejnovic
 *  Version: 0.1
 *
 *  The MIT License (MIT)
 *
 *  Copyright 2017 Marko Vejnovic
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation
 *  the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *  and/or sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.
 */

#include "connectionFunctions.h"
#include "helperFunctions.h"
#include <AccelStepper.h>

#define DIR_LEFT 1
#define DIR_RIGHT 0

#define SPEED 800

int dir, curPos = 0;
AccelStepper stepper(AccelStepper::DRIVER, 9, 8);

void sdCmdHandler(int direction)
{
	dir = direction;
}

void ssCmdHandler()
{
	if (dir == DIR_LEFT)
	{
		stepper.move(1);
		stepper.runToPosition();
	}
	else if (dir == DIR_RIGHT)
	{
		stepper.move(-1);
		stepper.runToPosition();
	}
}

void msCmdHandler(long steps)
{
	if (dir == DIR_LEFT)
	{
		stepper.move(steps);
		stepper.runToPosition();
	}
	else if (dir == DIR_RIGHT)
	{
		stepper.move(-steps);
		stepper.runToPosition();
	}
}

struct commandHandlers cmdHandlers =
{
	.setDirectionCommandHandler = sdCmdHandler,
	.singleStepCommandHandler = ssCmdHandler,
	.multipleStepCommandHandler = msCmdHandler
};

void setup()
{
	Serial.begin(9600);

	stepper.setMaxSpeed(SPEED);
	stepper.setAcceleration(SPEED / 10);

	waitForConnection();
}

void loop()
{
	processCommand(cmdHandlers);
}
