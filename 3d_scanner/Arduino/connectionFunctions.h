/**
 *  Created by Marko Vejnovic
 *  Version: 0.1
 *
 *  The MIT License (MIT)
 *
 *  Copyright 2017 Marko Vejnovic
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation
 *  the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *  and/or sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.
 */

/** This file contains functions which handle and process serial IO using a
 *  custom protocol.
 *
 *  In order to connect to the Arduino, you must send CONNECTION_SYN\n. The
 *  Arduino will then respond with CONNECTION_ACK\n, to which you must reply with
 *  CONNECTION_OK\n.
 *
 *  It is only then that you will be able to send commands to the Arduino.
 *
 *  Currently, this firmware supports the following commands:
 *  	COMMAND_SET_DIRECTION\nvalue\n (value must be 1 or 0)
 *  	COMMAND_SINGLE_STEP\n
 *  	COMMAND_MULTIPLE_STEP\nvalue\n (value must be a number in the following
 *   									format: abcdef, where every letter is
 *  									a digit (eg. 004372))
 *
 *  In order for these commands to do something, you must create a
 *  commandHandlers struct with the appropriate handler functions, and pass
 *  it to processCommand(struct commandHandlers), which is the parser function.
 */

#ifndef ARDUINO_H
#include <Arduino.h>
#define ARDUINO_H
#endif

#ifndef HELPER_FUNCTIONS_H
#include "helperFunctions.h"
#define HELPER_FUNCTIONS_H
#endif

/** Connection signals constants.
 *  These are used to interpret signals received and sent to the host machine.
 *  They need to be the same on the host machine.
 */
#define UNKOWN_COMMAND_ERROR -2
#define ERROR -1
#define CONNECTION_SYN 0
#define CONNECTION_ACK 1
#define CONNECTION_OK 2
#define GENERAL_ACK 3
#define GENERAL_OK 4
#define COMMAND_SET_DIRECTION 5
#define COMMAND_SINGLE_STEP 6
#define COMMAND_MULTIPLE_STEP 7


/** Command handlers.
 *  This struct needs to be passed to processCommand(struct commandHandlers) in
 *  order for the struct's members to handle the data for different signals.
 *  Every time a command is triggered, its corresponding handler (passed as a
 *  member of the struct) is called.
 */
struct commandHandlers
{
	void (*setDirectionCommandHandler) (int direction); // Called on COMMAND_SET_DIRECTION
	void (*singleStepCommandHandler) (void); // Called on COMMAND_SINGLE_STEP
	void (*multipleStepCommandHandler) (long steps); // Called on COMMAND_MULTIPLE_STEP
};

/** Waits for the connection signal.
 *  This function checks the received signal against predefined values and 
 *  blocks further code execution until the proper signal is given
 */
void waitForConnection();

/** Tries to process a single received command
 *  This function blocks code execution in order to process received serial
 *  input.
 */
void processCommand(struct commandHandlers cmdHandlers);
