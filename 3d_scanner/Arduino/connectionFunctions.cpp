/**
 *  Created by Marko Vejnovic
 *  Version: 0.1
 *
 *  The MIT License (MIT)
 *
 *  Copyright 2017 Marko Vejnovic
 *
 *  Permission is hereby granted, free of charge, to any person obtaining a
 *  copy of this software and associated documentation files (the "Software"),
 *  to deal in the Software without restriction, including without limitation
 *  the rights to use, copy, modify, merge, publish, distribute, sublicense,
 *  and/or sell copies of the Software, and to permit persons to whom the
 *  Software is furnished to do so, subject to the following conditions:
 *
 *  The above copyright notice and this permission notice shall be included in
 *  all copies or substantial portions of the Software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 *  FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
 *  DEALINGS IN THE SOFTWARE.
 */

#include "connectionFunctions.h"

void waitForConnection()
{
	char lastChar;

	// Wait for CONNECTION_SYN
	while (1)
	{
		if (Serial.available() > 0)
		{
			char currentChar = Serial.read();

			if (currentChar == '\n')
			{
				if (lastChar - '0' == CONNECTION_SYN)
				{
					Serial.println(CONNECTION_ACK);

					// Wait for CONNECTION_OK
					while (1)
					{
						if (Serial.available() > 0)
						{
							currentChar = Serial.read();

							if (currentChar == '\n')
							{
								if (lastChar - '0' == CONNECTION_OK)
								{
									break;
								}
							}

							lastChar = currentChar;
						}
					}
					break;
				}
			}

			lastChar = currentChar;
		}
	}
}

void processCommand(struct commandHandlers cmdHandlers)
{
	char lastChar;

	while (1)
	{
		if (Serial.available() > 0)
		{
			char currentChar = Serial.read();
			if (currentChar == '\n')
			{
				switch (lastChar - '0')
				{
					case(COMMAND_SET_DIRECTION):
						int direction;

						while (1)
						{
							char lastChar;
							if (Serial.available() > 0)
							{
								char currentChar = Serial.read();

								if (currentChar == '\n')
								{
									direction = lastChar - '0';
									Serial.println(GENERAL_ACK);
									break;
								}

								lastChar = currentChar;
							}
						}

						cmdHandlers.setDirectionCommandHandler(direction);
						Serial.println(GENERAL_OK);
						break;

					case(COMMAND_SINGLE_STEP):
						Serial.println(GENERAL_ACK);
						cmdHandlers.singleStepCommandHandler();
						Serial.println(GENERAL_OK);
						break;

					case(COMMAND_MULTIPLE_STEP):
					{
						int stepsA[6] = {-1, -1, -1, -1, -1, -1};
						int i = 0;

						while (1)
						{
							if (Serial.available() > 0)
							{
								char currentChar = Serial.read();

								if (currentChar == '\n')
								{
									if (intArrayHasNegative(stepsA, 6))
										Serial.println(ERROR);
									else
										Serial.println(GENERAL_ACK);
									break;
								}

								stepsA[i] = currentChar - '0';

								i = (i == 5) ? 0 : i + 1;
							}
						}

						int steps = 100000 * stepsA[0] + 10000 * stepsA[1] +
							1000 * stepsA[2] + 100 * stepsA[3] +
							10 * stepsA[4] + stepsA[5];

						cmdHandlers.multipleStepCommandHandler(steps);
						Serial.println(GENERAL_OK);
						break;
					}

					default:
						Serial.println(UNKOWN_COMMAND_ERROR);
						break;
				}
			}

			lastChar = currentChar;
		}
	}
}
